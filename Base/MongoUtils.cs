﻿using System;
using System.Collections.Generic;
using System.Linq;
using Base.System;
using MongoDB.Driver;

namespace Base
{
    internal static class MongoUtils
    {
        private static readonly Dictionary<Type, string> CollectionNames = new Dictionary<Type, string>();
        
        private static readonly object SyncObj = new object();

        private static string GetCollectionName(Type type)
        {
            string name;
            if (CollectionNames.TryGetValue(type, out name))
                return name;

            lock (SyncObj)
            {
                var attr =
                    (CollectionNameAttribute)
                        type.GetCustomAttributes(typeof(CollectionNameAttribute), true).FirstOrDefault();
                name = attr == null ? type.Name : attr.Name;
                CollectionNames[type] = name;
            }
            return name;
        }

        public static IMongoCollection<TEntity> GetCollection<TEntity>(MongoUrl url)
        {
            var client =
                new MongoClient(new MongoClientSettings
                {
                    ConnectTimeout = TimeSpan.FromSeconds(30),
                    SocketTimeout = TimeSpan.FromSeconds(30),
                    WaitQueueTimeout = TimeSpan.FromSeconds(30),
                    MaxConnectionIdleTime = TimeSpan.FromSeconds(45),
                    Server = url.Server
                });
            var db = client.GetDatabase(url.DatabaseName);
            return db.GetCollection<TEntity>(GetCollectionName(typeof(TEntity)));
        }
    }
}
