﻿namespace Base.Enum
{
    public enum EntityState
    {
        Active,
        OnDeletion,
        Deleted,
        Deactivated
    }
}
