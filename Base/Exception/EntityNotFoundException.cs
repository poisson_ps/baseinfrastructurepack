﻿using System;

namespace Base.Exception
{
    public class EntityNotFoundException : global::System.Exception
    {
        public EntityNotFoundException()
        {
        }

        public EntityNotFoundException(Type type, object id) :
            base(string.Format("Entity of type {0} was not found by {1}", type.FullName, id))
        {
        }
    }

    public class EntityExistsException : global::System.Exception
    {
        public EntityExistsException()
        {
        }

        public EntityExistsException(Type type, object id) :
            base(string.Format("Entity of type {0} was not found by {1}", type.FullName, id))
        {
        }
    }
}
