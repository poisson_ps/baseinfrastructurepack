﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Base.Dal;
using Base.Entity;
using Microsoft.AspNet.Identity;
using MongoDB.Bson;

namespace Base.System
{
    public class UserStore : IUserPasswordStore<IdentityUser, ObjectId>,
      IUserRoleStore<IdentityUser, ObjectId>,
      IUserLoginStore<IdentityUser, ObjectId>,
      IUserSecurityStampStore<IdentityUser, ObjectId>,
      IUserEmailStore<IdentityUser, ObjectId>,
      IUserPhoneNumberStore<IdentityUser, ObjectId>,
      IUserTwoFactorStore<IdentityUser, ObjectId>,
      IUserLockoutStore<IdentityUser, ObjectId>
    {
        private readonly IDal<IdentityUser> _dal;

        public UserStore(IDal<IdentityUser> dal)
        {
            _dal = dal;
        }

        public virtual void Dispose()
        {
            _dal?.Dispose();
        }

        public virtual Task CreateAsync(IdentityUser user)
        {
            return _dal.InsertAsync(user);
        }

        public virtual Task UpdateAsync(IdentityUser user)
        {
            return _dal.UpdateAsync(user.Id, user);
        }

        public virtual Task DeleteAsync(IdentityUser user)
        {
            return _dal.DeleteAsync(user.Id);
        }

        public virtual Task<IdentityUser> FindByIdAsync(ObjectId userId)
        {
            return _dal.GetAsync(userId);
        }

        public virtual async Task<IdentityUser> FindByNameAsync(string userName)
        {
            return (await _dal.GetAsync(u => u.UserName == userName, null, new PagingInfo { Take = 1 })).FirstOrDefault();
        }

        public virtual Task SetPasswordHashAsync(IdentityUser user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        public virtual Task<string> GetPasswordHashAsync(IdentityUser user)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public virtual Task<bool> HasPasswordAsync(IdentityUser user)
        {
            return Task.FromResult(user.HasPassword());
        }

        public virtual Task AddToRoleAsync(IdentityUser user, string roleName)
        {
            user.AddRole(roleName);
            return Task.FromResult(0);
        }

        public virtual Task RemoveFromRoleAsync(IdentityUser user, string roleName)
        {
            user.RemoveRole(roleName);
            return Task.FromResult(0);
        }

        public virtual Task<IList<string>> GetRolesAsync(IdentityUser user)
        {
            return Task.FromResult((IList<string>)user.Roles);
        }

        public virtual Task<bool> IsInRoleAsync(IdentityUser user, string roleName)
        {
            return Task.FromResult(user.Roles.Contains(roleName));
        }

        public virtual Task AddLoginAsync(IdentityUser user, UserLoginInfo login)
        {
            user.AddLogin(login);
            return Task.FromResult(0);
        }

        public virtual Task RemoveLoginAsync(IdentityUser user, UserLoginInfo login)
        {
            user.RemoveLogin(login);
            return Task.FromResult(0);
        }

        public virtual Task<IList<UserLoginInfo>> GetLoginsAsync(IdentityUser user)
        {
            return Task.FromResult((IList<UserLoginInfo>)user.Logins);
        }

        public virtual async Task<IdentityUser> FindAsync(UserLoginInfo login)
        {
            return
                (await
                    _dal.GetAsync(
                        u =>
                            u.Logins.Any(
                                l => l.LoginProvider == login.LoginProvider && l.ProviderKey == login.ProviderKey), null,
                        new PagingInfo { Take = 1 })).FirstOrDefault();
        }

        public virtual Task SetSecurityStampAsync(IdentityUser user, string stamp)
        {
            user.SecurityStamp = stamp;
            return Task.FromResult(0);
        }

        public virtual Task<string> GetSecurityStampAsync(IdentityUser user)
        {
            return Task.FromResult(user.SecurityStamp);
        }

        public virtual Task<bool> GetEmailConfirmedAsync(IdentityUser user)
        {
            return Task.FromResult(user.EmailConfirmed);
        }

        public virtual Task SetEmailConfirmedAsync(IdentityUser user, bool confirmed)
        {
            user.EmailConfirmed = confirmed;
            return Task.FromResult(0);
        }

        public virtual Task SetEmailAsync(IdentityUser user, string email)
        {
            user.Email = email;
            return Task.FromResult(0);
        }

        public virtual Task<string> GetEmailAsync(IdentityUser user)
        {
            return Task.FromResult(user.Email);
        }

        public virtual async Task<IdentityUser> FindByEmailAsync(string email)
        {
            return (await _dal.GetAsync(u => u.Email == email, null, new PagingInfo { Take = 1 })).FirstOrDefault();
        }

        public virtual Task SetPhoneNumberAsync(IdentityUser user, string phoneNumber)
        {
            user.PhoneNumber = phoneNumber;
            return Task.FromResult(0);
        }

        public virtual Task<string> GetPhoneNumberAsync(IdentityUser user)
        {
            return Task.FromResult(user.PhoneNumber);
        }

        public virtual Task<bool> GetPhoneNumberConfirmedAsync(IdentityUser user)
        {
            return Task.FromResult(user.PhoneNumberConfirmed);
        }

        public virtual Task SetPhoneNumberConfirmedAsync(IdentityUser user, bool confirmed)
        {
            user.PhoneNumberConfirmed = confirmed;
            return Task.FromResult(0);
        }

        public virtual Task SetTwoFactorEnabledAsync(IdentityUser user, bool enabled)
        {
            user.TwoFactorEnabled = enabled;
            return Task.FromResult(0);
        }

        public virtual Task<bool> GetTwoFactorEnabledAsync(IdentityUser user)
        {
            return Task.FromResult(user.TwoFactorEnabled);
        }

        public virtual Task<DateTimeOffset> GetLockoutEndDateAsync(IdentityUser user)
        {
            return Task.FromResult(user.LockoutEndDateUtc ?? new DateTimeOffset());
        }

        public virtual Task SetLockoutEndDateAsync(IdentityUser user, DateTimeOffset lockoutEnd)
        {
            user.LockoutEndDateUtc = new DateTime(lockoutEnd.Ticks, DateTimeKind.Utc);
            return Task.FromResult(0);
        }

        public virtual Task<int> IncrementAccessFailedCountAsync(IdentityUser user)
        {
            user.AccessFailedCount++;
            return Task.FromResult(user.AccessFailedCount);
        }

        public virtual Task ResetAccessFailedCountAsync(IdentityUser user)
        {
            user.AccessFailedCount = 0;
            return Task.FromResult(0);
        }

        public virtual Task<int> GetAccessFailedCountAsync(IdentityUser user)
        {
            return Task.FromResult(user.AccessFailedCount);
        }

        public virtual Task<bool> GetLockoutEnabledAsync(IdentityUser user)
        {
            return Task.FromResult(user.LockoutEnabled);
        }

        public virtual Task SetLockoutEnabledAsync(IdentityUser user, bool enabled)
        {
            user.LockoutEnabled = enabled;
            return Task.FromResult(0);
        }
    }
}
