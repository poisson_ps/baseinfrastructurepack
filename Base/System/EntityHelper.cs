﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Base.System
{
    public static class EntityHelper
    {
        private static string ParseExpression(Expression expression)
        {
            var unaryExpression = expression as UnaryExpression;
            if (unaryExpression != null)
            {
                if (unaryExpression.NodeType == ExpressionType.Convert)
                {
                    var ex = unaryExpression.Operand;
                    var mex = (MemberExpression)ex;
                    return mex.Member.Name;
                }
            }
            else
            {
                var memberExpression = expression as MemberExpression;
                if (memberExpression != null)
                {
                    var memberExpressionOrg = memberExpression;

                    var path = "";

                    while (memberExpression != null && memberExpression.Expression.NodeType == ExpressionType.MemberAccess)
                    {
                        var propInfo = memberExpression.Expression
                            .GetType().GetProperty("Member");
                        var propValue = propInfo.GetValue(memberExpression.Expression, null) as PropertyInfo;
                        path = propValue.Name + "." + path;

                        memberExpression = memberExpression.Expression as MemberExpression;
                    }

                    return path + memberExpressionOrg.Member.Name;
                }
            }
            throw new ArgumentException(string.Format("Expression should be of type: UnaryExpression, MemberExpression"));
        }

        private static Type ExpressionLastType(Expression expression)
        {
            var unaryExpression = expression as UnaryExpression;
            if (unaryExpression != null)
            {
                if (unaryExpression.NodeType == ExpressionType.Convert)
                {
                    var ex = unaryExpression.Operand;
                    var mex = (MemberExpression)ex;
                    return mex.Member.DeclaringType;
                }
            }
            else
            {
                var memberExpression = expression as MemberExpression;
                if (memberExpression != null)
                {
                    var memberExpressionOrg = memberExpression;

                    while (memberExpression != null && memberExpression.Expression.NodeType == ExpressionType.MemberAccess)
                    {
                        memberExpression = memberExpression.Expression as MemberExpression;
                    }

                    return memberExpressionOrg.Type;
                }
            }
            return null;
        }

        public static string GetPropertyName<TEntity>(Expression<Func<TEntity, object>> expression)
        {
            return ParseExpression(expression.Body);
        }

        public static string GetPropertyName<TEntity, TPoperty>(Expression<Func<TEntity, TPoperty>> expression)
        {
            return ParseExpression(expression.Body);
        }

        public static Type GetPropertyType<T>(Expression<Func<T, object>> expression)
        {
            return ExpressionLastType(expression.Body);
        }
    }
}
