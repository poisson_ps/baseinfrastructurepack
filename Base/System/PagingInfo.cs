﻿namespace Base.System
{
    public class PagingInfo
    {
        public PagingInfo() { }

        public PagingInfo(int skip, int take)
        {
            Take = take;
            Skip = skip;
        }

        public int? Take { get; set; }

        public int? Skip { get; set; }
    }
}
