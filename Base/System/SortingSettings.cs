﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Base.System
{
    public class SortingSettings<TEntity>
    {
        public SortingSettings()
        {
            Items = new List<SortingItem>();
        }

        public List<SortingItem> Items { get; set; }

        public string Language { get; set; }

        public class SortingItem
        {
            public bool Asc { get; set; }

            public Expression<Func<TEntity, object>> Expression { get; set; }
        }
    }
}
