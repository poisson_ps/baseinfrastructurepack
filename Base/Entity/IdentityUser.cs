﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Base.Entity
{
    public class IdentityUser : DbEntity, IUser<ObjectId>
    {
        public IdentityUser(IdentityUser user)
        {
            Email = user.Email;
            PhoneNumber = user.PhoneNumber;
            Roles = user.Roles;
        }

        public IdentityUser()
        {
            Id = ObjectId.GenerateNewId();
            Roles = new List<string>();
            Logins = new List<UserLoginInfo>();
        }

        [BsonRequired]
        public ObjectId UserId { get; set; }

        [BsonRequired]
        public string UserName { get; set; }

        public string SecurityStamp { get; set; }

        [BsonIgnoreIfNull]
        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        public string PhoneNumber { get; set; }

        public bool PhoneNumberConfirmed { get; set; }

        public bool TwoFactorEnabled { get; set; }

        public DateTime? LockoutEndDateUtc { get; set; }

        public bool LockoutEnabled { get; set; }

        public int AccessFailedCount { get; set; }

        [BsonIgnoreIfNull]
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime? ExpireAt { get; set; }

        [BsonIgnoreIfNull]
        public List<string> Roles { get; set; }

        public void AddRole(string role)
        {
            Roles.Add(role);
        }

        public void RemoveRole(string role)
        {
            Roles.Remove(role);
        }

        [BsonIgnoreIfNull]
        public string PasswordHash { get; set; }

        [BsonIgnoreIfNull]
        public List<UserLoginInfo> Logins { get; set; }

        public void AddLogin(UserLoginInfo login)
        {
            Logins.Add(login);
        }

        public void RemoveLogin(UserLoginInfo login)
        {
            var loginsToRemove = Logins
                .Where(l => l.LoginProvider == login.LoginProvider)
                .Where(l => l.ProviderKey == login.ProviderKey);

            Logins = Logins.Except(loginsToRemove).ToList();
        }

        public bool HasPassword()
        {
            return false;
        }
    }

    public class IdentityUserClaim
    {
        public IdentityUserClaim()
        {
        }

        public IdentityUserClaim(Claim claim)
        {
            Type = claim.Type;
            Value = claim.Value;
        }

        public string Type { get; set; }
        public string Value { get; set; }

        public Claim ToSecurityClaim()
        {
            return new Claim(Type, Value);
        }
    }
}
