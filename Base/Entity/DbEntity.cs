﻿using System;
using Base.Enum;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Base.Entity
{
    public abstract class DbEntity
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        [BsonRequired]
        public DateTime CreatedAt { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime ModifiedAt { get; set; }

        private EntityState _state;

        [BsonDefaultValue(EntityState.Active)]
        public EntityState State
        {
            get { return _state; }
            set
            {
                _state = value;
                if (value == EntityState.OnDeletion)
                    DeleteAt = DateTime.Now.AddDays(7);
            }
        }

        [BsonIgnoreIfNull]
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime? DeleteAt { get; set; }
    }
}
