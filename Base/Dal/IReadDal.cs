﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Base.Entity;
using Base.System;
using MongoDB.Bson;

namespace Base.Dal
{
    public interface IReadDal<TEntity> : IDisposable
       where TEntity : DbEntity
    {
        Task<TEntity> GetAsync(ObjectId id);

        Task<List<TEntity>> GetAsync(
            Expression<Func<TEntity, bool>> filter = null,
            SortingSettings<TEntity> orderBy = null, PagingInfo pagingInfo = null);
    }
}
