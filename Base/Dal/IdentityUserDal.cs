﻿using Base.Entity;

namespace Base.Dal
{
    public class IdentityUserDal : BaseDal<IdentityUser>
    {
        public IdentityUserDal() { }

        public IdentityUserDal(string connectionString)
            : base(connectionString)
        {
        }
    }
}
