﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Base.Entity;
using Base.Enum;
using Base.Exception;
using Base.System;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Base.Dal
{
    public abstract class BaseDal<TEntity> : IDal<TEntity>
       where TEntity : DbEntity
    {
        static BaseDal()
        {
            MongoDefaults.GuidRepresentation = GuidRepresentation.CSharpLegacy;
        }

        protected readonly IMongoCollection<TEntity> Collection;

        protected BaseDal() : this(ConfigurationManager.ConnectionStrings["MongoCs"].ConnectionString) { }

        protected BaseDal(string connectionString)
        {
            Collection = MongoUtils.GetCollection<TEntity>(new MongoUrl(connectionString));
        }

        public virtual void Dispose()
        {
        }

        private static FilterDefinitionBuilder<TEntity> Filter { get { return Builders<TEntity>.Filter; } }

        private static UpdateDefinitionBuilder<TEntity> Update { get { return Builders<TEntity>.Update; } }

        public virtual async Task<TEntity> InsertAsync(TEntity entity)
        {
            if (entity == null)
                throw new NullReferenceException("Entity is required");

            entity.ModifiedAt = entity.CreatedAt = DateTime.UtcNow;
            await Collection.InsertOneAsync(entity).ConfigureAwait(false);
            return entity;
        }

        public async Task<List<TEntity>> InsertAsync(List<TEntity> entities)
        {
            if (entities == null)
                throw new NullReferenceException("Entities is required");

            entities.ForEach(entity => entity.ModifiedAt = entity.CreatedAt = DateTime.UtcNow);

            await Collection.InsertManyAsync(entities).ConfigureAwait(false);
            return entities;
        }

        public virtual async Task<TEntity> UpdateAsync(ObjectId id, TEntity entity)
        {
            if (entity == null)
                throw new NullReferenceException("Entity is required");

            entity.ModifiedAt = DateTime.UtcNow;
            var result = await Collection.ReplaceOneAsync(v => v.Id.Equals(id), entity).ConfigureAwait(false);

            if (result.IsAcknowledged && result.ModifiedCount == 0)
                throw new EntityNotFoundException(typeof(TEntity), id);

            return entity;
        }

        public async Task UpdateAsync(IList<TEntity> entities)
        {
            if (entities == null)
                throw new NullReferenceException("Entities is required");

            if (entities.Count == 0)
                return;

            foreach (var entity in entities)
            {
                entity.ModifiedAt = DateTime.UtcNow;
            }
            var result = await Collection.BulkWriteAsync(
                entities.Select(v => new ReplaceOneModel<TEntity>(Filter.Eq(f => f.Id, v.Id), v)),
                new BulkWriteOptions { IsOrdered = false }).ConfigureAwait(false);

            if (result.IsAcknowledged && result.ModifiedCount != entities.Count)
                throw new EntityNotFoundException(typeof(TEntity), string.Join(",", entities.Select(v => v.Id)));
        }

        public async Task<long> SetAsync<TProperty>(Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, TProperty>> property, TProperty value)
        {
            var result = await
                Collection.UpdateManyAsync(Filter.Where(filter), Update.Set(property, value).Set(v => v.ModifiedAt, DateTime.UtcNow));
            return result.ModifiedCount;
        }

        public virtual async Task DeleteAsync(TEntity entity)
        {
            var result = await Collection.DeleteOneAsync(v => v.Id.Equals(entity.Id)).ConfigureAwait(false);

            if (result.IsAcknowledged && result.DeletedCount == 0)
                throw new EntityNotFoundException(typeof(TEntity), entity.Id);
        }

        public virtual async Task DeleteAsync(ObjectId id)
        {
            var result = await Collection.DeleteOneAsync(v => v.Id.Equals(id)).ConfigureAwait(false);

            if (result.IsAcknowledged && result.DeletedCount == 0)
                throw new EntityNotFoundException(typeof(TEntity), id);
        }
       
        public async Task SoftDelete(ObjectId id)
        {
            var result = await SoftDelete(v => v.Id == id);
            if (result == 0)
                throw new EntityNotFoundException(typeof(TEntity), id);
        }

        public async Task<long> SoftDelete(Expression<Func<TEntity, bool>> filter)
        {
            return await SoftDelete(filter, DateTime.UtcNow, EntityState.Deleted);
        }

        public async Task SoftDelete(ObjectId id, DateTime deleteOn)
        {
            var result = await SoftDelete(v => v.Id == id, deleteOn);
            if (result == 0)
                throw new EntityNotFoundException(typeof(TEntity), id);
        }

        public async Task<long> SoftDelete(Expression<Func<TEntity, bool>> filter, DateTime deleteOn)
        {
            return await SoftDelete(filter, deleteOn, EntityState.OnDeletion);
        }

        private async Task<long> SoftDelete(Expression<Func<TEntity, bool>> filter, DateTime deleteOn, EntityState state)
        {
            var result = await
                Collection.UpdateManyAsync(filter, Update.Set(v => v.State, state).Set(v => v.DeleteAt, deleteOn));
            return result.ModifiedCount;
        }

        public virtual async Task<TEntity> GetAsync(ObjectId id)
        {
            return await Collection.Find(v => v.Id.Equals(id)).FirstOrDefaultAsync().ConfigureAwait(false);
        }

        public virtual async Task<List<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null,
            SortingSettings<TEntity> orderBy = null, PagingInfo pagingInfo = null)
        {
            var queryDefinition = Filter.Where(v => v.State != EntityState.Deleted);
            if (filter != null)
                queryDefinition = Filter.And(queryDefinition, Filter.Where(filter));

            var query = Collection.Find(queryDefinition);

            if (orderBy != null && orderBy.Items.Count > 0)
            {
                var sort = BuildSorting(orderBy);
                query = query.Sort(sort);
            }

            if (pagingInfo != null)
            {
                if (pagingInfo.Skip.HasValue)
                    query = query.Skip(pagingInfo.Skip.Value);
                if (pagingInfo.Take.HasValue)
                    query = query.Limit(pagingInfo.Take.Value);
            }

            return await query.ToListAsync().ConfigureAwait(false);
        }

        private static SortDefinition<TEntity> BuildSorting(SortingSettings<TEntity> orderBy)
        {
            var builder = Builders<TEntity>.Sort;
            var sorts = new List<SortDefinition<TEntity>>(orderBy.Items.Count);
            foreach (var sortingSetting in orderBy.Items)
            {
                SortDefinition<TEntity> sort = null;
                if (!string.IsNullOrEmpty(orderBy.Language) &&
                    EntityHelper.GetPropertyType(sortingSetting.Expression) == typeof(MultiLangText))
                {
                    var propertyName = EntityHelper.GetPropertyName(sortingSetting.Expression);
                    if (!string.IsNullOrEmpty(propertyName))
                    {
                        propertyName = propertyName + $".{orderBy.Language}";
                        sort = sortingSetting.Asc
                            ? builder.Ascending(propertyName)
                            : builder.Descending(propertyName);

                    }
                }
                else
                {
                    sort = sortingSetting.Asc
                        ? Builders<TEntity>.Sort.Ascending(sortingSetting.Expression)
                        : Builders<TEntity>.Sort.Descending(sortingSetting.Expression);
                }
                sorts.Add(sort);
            }
            return builder.Combine(sorts.Where(v => v != null));
        }
    }
}
