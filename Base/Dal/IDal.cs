﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Base.Entity;
using MongoDB.Bson;

namespace Base.Dal
{
    public interface IDal<TEntity> : IReadDal<TEntity>
        where TEntity : DbEntity
    {
        Task<TEntity> InsertAsync(TEntity entity);

        Task<List<TEntity>> InsertAsync(List<TEntity> entities);

        Task<TEntity> UpdateAsync(ObjectId id, TEntity entity);

        Task UpdateAsync(IList<TEntity> entities);

        Task<long> SetAsync<TProperty>(Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, TProperty>> property,
            TProperty value);

        Task DeleteAsync(TEntity entity);

        Task DeleteAsync(ObjectId id);

        Task SoftDelete(ObjectId id);

        Task<long> SoftDelete(Expression<Func<TEntity, bool>> filter);

        Task SoftDelete(ObjectId id, DateTime deleteOn);

        Task<long> SoftDelete(Expression<Func<TEntity, bool>> filter, DateTime deleteOn);
    }
}
